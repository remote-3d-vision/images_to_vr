//window creation and message handling
#include "window.h"
//image rendering and warping
#include "render.h"
//shared memory image buffer
#include "image_buffer/image_buffer.h"

//include the basic windows header files
#include <windows.h>
#include <windowsx.h>

//include threading stuff
#include <process.h>

//function prototypes
void image_reader_thread_left(void *);
void image_reader_thread_right(void *);

//shared memory images variables
image_buffer_reader image_reader_left;
image_buffer_reader image_reader_right;
unsigned char *left_image;
unsigned char *right_image;
image_info_t *image_info_left;
image_info_t *image_info_right;

// the entry point for any Windows program
int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPSTR lpCmdLine,
                   int nCmdShow)
{
    //debug console
    create_console();

    HWND hWnd;

    InitWindow(&hWnd, hInstance, L"Images to VR", 150, 150, SCREEN_WIDTH, SCREEN_HEIGHT);
     
    ShowWindow(hWnd, nCmdShow);

    //initialize our shared memory reader if there is shared memory available
    if (image_reader_left.init_left() < 0)
	{
        MessageBox(NULL, L"A program must open the shared memory first", 
			       L"Shared Memory Error", 
				   MB_OK | MB_ICONERROR);
        return -1;
	}
    if (image_reader_right.init_right() < 0)
	{
        MessageBox(NULL, L"A program must open the shared memory first", 
			       L"Shared Memory Error", 
				   MB_OK | MB_ICONERROR);
        return -1;
	}

    //XXX added to let render happen if shared mem is open, but nothing
    //is going to be written
	image_info_t placeholder;
    placeholder.width = 640;
    placeholder.height = 480;
    placeholder.size = 640*480*4;
    placeholder.buffer_index = 0;

    image_info_left = &placeholder;
    image_info_right = &placeholder;

    //read images from shared memory in the background
    _beginthread(image_reader_thread_left, 0, NULL);
    _beginthread(image_reader_thread_right, 0, NULL);

    // set up and initialize Direct3D
    InitD3D(hWnd);

    // enter the main loop
    MSG msg;
    while(TRUE)
    {
        //handle messages
        if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);

            if(msg.message == WM_QUIT)
                break;
        }

        RenderFrame(image_info_left, image_info_right, left_image, right_image);
    }

    // clean up DirectX and COM
    CleanD3D();

    return msg.wParam;
}

//continuously assign *_image pointers to the most recently updated image buffers in shared memory
void image_reader_thread_left(void *arg)
{
    while(true)
	{
        image_reader_left.get_image_buffer(&image_info_left, &left_image);
	}
}

void image_reader_thread_right(void *arg)
{
    while(true)
	{
        image_reader_right.get_image_buffer(&image_info_right, &right_image);
	}
}