
#include "keyboard.h"
#include <Windows.h>
#include <stdio.h>

Keyboard::Keyboard()
{
    current_state = keyboard_state_1;
    previous_state = keyboard_state_2;
}

bool Keyboard::key_pressed(BYTE vk_code)
{
    //rising edge / key down
    if ((current_state[vk_code] & 0x80) && !(previous_state[vk_code] & 0x80))
	{
        return true;
	}
	else
	{
        return false;
	}
}

void Keyboard::get_keyboard_state()
{
    BYTE *temp = previous_state;
    previous_state = current_state;
    current_state = temp;
    //sets an array of 256 bytes representing 256 keys, equal to the state of that key
    GetKeyboardState(current_state);
}
