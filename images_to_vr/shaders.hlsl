cbuffer ConstantBuffer : register(b0)
{
    float4x4 matFinal;
};

//total struct size must be a multiple of 16 bytes
cbuffer hmd_params_cbuffer : register(b1)
{
    //warp adjustments

    //where the warping is centered around
    float2 lens_offset_left;
    float2 lens_offset_right;
    //how large the texture is 
    float2 texture_scale_left;
    float2 texture_scale_right;
    //how warped the image is
    float4 k_values_left;
    float4 k_values_right;
    
    //texture adjustments

    //where the textures are centered
    float2 texture_offset_left;
    float2 texture_offset_right;
};

Texture2D Texture;
SamplerState ss;

struct VOut
{
    float2 texcoord : TEXCOORD;
    float4 position : SV_POSITION;
};

VOut VShader(float4 position : POSITION, float2 texcoord : TEXCOORD)
{
    VOut output;

    output.position = mul(matFinal, position);
    output.texcoord = texcoord;

    return output;
}

float2 hmd_warp(float2 tex_coord, float2 lens_offset, float2 texture_scale, float4 k_values)
{
    //texture ranges from (0,1) in both x and y so the middle would be 0.5 in both directions
    float2 lens_center = float2(0.5f, 0.5f);
    /*
        texture goes from (0,1), then we shift by 0.5 to the side for centering
        so texture now goes from (-0.5, to 0.5) then multiply by 2 for x direction,
        then multiply by 2/1.333 for the y direction. (1.333 is the aspect ratio for 640x480)
    */
    float2 theta = (tex_coord - (lens_center + lens_offset)) * float2(2.0f, 1.5f); //scale to [-1, 1]
    float2 r_squared = theta.x * theta.x + theta.y * theta.y;
    float2 r_vector = theta * (k_values.x +
                              k_values.y * r_squared +
                              k_values.z * r_squared * r_squared +
                              k_values.w * r_squared * r_squared * r_squared);

    //instead of using the actual texture coordinate, return the texture coordinate that's a distance
    //away according the the texture scale and r function described in the Oculus SDK. This gives the 
    //effect of a scaled texture with lens warping
    return tex_coord + texture_scale * r_vector;
}

float4 pixel_shader_left(float2 tex_coord : TEXCOORD) : SV_TARGET
{
    float2 tc = hmd_warp(tex_coord, lens_offset_left, texture_scale_left, k_values_left);

    //return black if outside the texture
    if (any(clamp(tc, float2(0,0), float2(1,1)) - tc))
        return 0;
    tc += texture_offset_left;
    return Texture.Sample(ss, tc);
}

float4 pixel_shader_right(float2 tex_coord : TEXCOORD) : SV_TARGET
{
    float2 tc = hmd_warp(tex_coord, lens_offset_right, texture_scale_right, k_values_right);

    //return black if outside the texture
    if (any(clamp(tc, float2(0,0), float2(1,1)) - tc))
        return 0;

    tc += texture_offset_right;
    return Texture.Sample(ss, tc);
}

float4 no_distortion(float2 texcoord : TEXCOORD) : SV_TARGET
{
    return Texture.Sample(ss, texcoord);
}
