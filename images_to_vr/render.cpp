#include "render.h"
#include "keyboard.h"
#include "window.h"

//include Direct3D header files
#include <d3d11.h>
#include <d3dx11.h>
#include <d3dx10.h>
#include <DirectXMath.h>//required for XMFLOAT*

//printf
#include <stdio.h>

//include the Direct3D Library file
#pragma comment (lib, "d3d11.lib")
#pragma comment (lib, "d3dx11.lib")
#pragma comment (lib, "d3dx10.lib")
#pragma comment (lib, "dxgi.lib")

//key press macro
#define KEY_DOWN(vk_code) ((GetAsyncKeyState(vk_code) & 0x8000) ? 1 : 0

//global delcarations
IDXGISwapChain *swapchain;             //the pointer to the swap chain interface
ID3D11Device *dev;                     //the pointer to our Direct3D device interface
ID3D11DeviceContext *devcon;           //the pointer to our Direct3D device context
ID3D11RenderTargetView *backbuffer;    //the pointer to our back buffer
ID3D11InputLayout *pLayout;            //the pointer to the input layout
ID3D11VertexShader *vertex_shader;     //the pointer to the vertex shader
ID3D11PixelShader *pixel_shader_left;                //the pointer to the pixel shader
ID3D11PixelShader *pixel_shader_right;                //the pointer to the pixel shader
ID3D11Buffer *pVBuffer;                //the pointer to the vertex buffer
ID3D11Buffer *pCBuffer;                //the pointer to the constant buffer
ID3D11Buffer *hmd_params_buffer;                //the pointer to the constant buffer

D3D11_VIEWPORT viewports[2];
ID3D11Texture2D* pLeftTexture;                  //left image texture to render to screen
ID3D11Texture2D* pRightTexture;                  //right image texture to render to screen
ID3D11ShaderResourceView *pLeftSRV;         //specifies the subresources (ID3D11Texture2D in our case) a shader can access during rendering
ID3D11ShaderResourceView *pRightSRV;         //specifies the subresources (ID3D11Texture2D in our case) a shader can access during rendering

// various buffer structs
struct VERTEX{FLOAT X, Y, Z, U, V;};    //X,Y,Z are 3D space coordinates, U,V are texture coordinates

typedef struct hmd_params_s
{
    DirectX::XMFLOAT2 lens_offset_left;
    DirectX::XMFLOAT2 lens_offset_right;
    DirectX::XMFLOAT2 texture_scale_left;
    DirectX::XMFLOAT2 texture_scale_right;
    DirectX::XMFLOAT4 k_values_left;
    DirectX::XMFLOAT4 k_values_right;
    DirectX::XMFLOAT2 texture_offset_left;
    DirectX::XMFLOAT2 texture_offset_right;
} hmd_params_t;

hmd_params_t hmd_params;
//controls whether or not the right side will mirror the left side's params
int mirroring = true;
int allow_adjustments = false;
int adjusting = ADJUST_TEXTURE_OFFSET_LEFT;

//function prototypes
void InitGraphics(void);                //creates the shape to render
void InitPipeline(void);    			//loads and prepares the shaders

//Create a keyboard handler
Keyboard *kb;

// this function initializes and prepares Direct3D for use
void InitD3D(HWND hWnd)
{
    kb = new Keyboard();

    // create a struct to hold information about the swap chain
    DXGI_SWAP_CHAIN_DESC scd;

    // clear out the struct for use
    ZeroMemory(&scd, sizeof(DXGI_SWAP_CHAIN_DESC));

    // fill the swap chain description struct
    scd.BufferCount = 1;                                   // one back buffer
    scd.BufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;    // use 32-bit color
    scd.BufferDesc.Width = SCREEN_WIDTH;                   // set the back buffer width
    scd.BufferDesc.Height = SCREEN_HEIGHT;                 // set the back buffer height
    scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;     // how swap chain is to be used
    scd.OutputWindow = hWnd;                               // the window to be used
    scd.SampleDesc.Count = 1;                              // how many multisamples
    scd.Windowed = WINDOWED;                                   // windowed/full-screen mode
    scd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;    // allow full-screen switching

    //Get the refresh rate of the monitor
    if (VSYNC)
	{
        IDXGIFactory *factory;
        IDXGIAdapter *adapter;
        IDXGIOutput *adapter_output;
        unsigned int num_modes, i, numerator, denominator;
        DXGI_MODE_DESC *display_mode_list;
        CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&factory);
        factory->EnumAdapters(0, &adapter);
        adapter->EnumOutputs(0, &adapter_output);
        adapter_output->GetDisplayModeList(DXGI_FORMAT_B8G8R8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &num_modes, NULL);
        display_mode_list = new DXGI_MODE_DESC[num_modes];
        adapter_output->GetDisplayModeList(DXGI_FORMAT_B8G8R8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &num_modes, display_mode_list);
        for (i = 0; i < num_modes; i++)
        {
            if (display_mode_list[i].Width == (unsigned int)SCREEN_WIDTH)
            {
                if (display_mode_list[i].Height == (unsigned int)SCREEN_HEIGHT)
                {
                    numerator = display_mode_list[i].RefreshRate.Numerator;
                    denominator = display_mode_list[i].RefreshRate.Denominator;
                }
            }
        }
        scd.BufferDesc.RefreshRate.Numerator = numerator;
        scd.BufferDesc.RefreshRate.Denominator = denominator;
        scd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
        scd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	}

    // create a device, device context and swap chain using the information in the scd struct
    D3D11CreateDeviceAndSwapChain(NULL,
                                  D3D_DRIVER_TYPE_HARDWARE,
                                  NULL,
                                  NULL,
                                  NULL,
                                  NULL,
                                  D3D11_SDK_VERSION,
                                  &scd,
                                  &swapchain,
                                  &dev,
                                  NULL,
                                  &devcon);


    // get the address of the back buffer
    ID3D11Texture2D *pBackBuffer;
    swapchain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);

    // use the back buffer address to create the render target
    dev->CreateRenderTargetView(pBackBuffer, NULL, &backbuffer);
    pBackBuffer->Release();

    // set the render target as the back buffer
    devcon->OMSetRenderTargets(1, &backbuffer, NULL);

    ZeroMemory(viewports, sizeof(D3D11_VIEWPORT)*2);

    //left view port
    viewports[0].TopLeftX = 0;
    viewports[0].TopLeftY = 0;
    viewports[0].Width = SCREEN_WIDTH / 2;
    viewports[0].Height = SCREEN_HEIGHT;

    //right view port
    viewports[1].TopLeftX = SCREEN_WIDTH / 2;
    viewports[1].TopLeftY = 0;
    viewports[1].Width = SCREEN_WIDTH / 2;
    viewports[1].Height = SCREEN_HEIGHT;

    devcon->RSSetViewports(2, viewports);

    InitPipeline();
    InitGraphics();
}

// this is the function that creates the shape to render
void InitGraphics()
{
    //create a rectangle 4:3 aspect ratio using the VERTEX struct
    /*
        example:
        If horizontal width = 640 and is represented by range from -1.0 to 1.0
        And vertical width = 800 then it is represented by range from -1.25 to 1.25
        
        The viewport is 640x800 and if we want to render a 640x480 then
        - 240/400*1.25 = 0.75 up and down from the vertical center

        FOV is vertical and is set to 45 degrees
        We want the rendered rectangle to take up the entire viewport so 
        1.25/tan(45/2) = 3.017 (distance from screen)
    */
    VERTEX rectangle_4_by_3[] =
    {
        //XYZ (vertex coordinates), UV (texture coordinates)
        //order matters
        {-1.0f,  0.75f, 3.017f, 0.0f, 0.0f},
        { 1.0f,  0.75f, 3.017f, 1.0f, 0.0f},
        {-1.0f, -0.75f, 3.017f, 0.0f, 1.0f},
        { 1.0f, -0.75f, 3.017f, 1.0f, 1.0f}
    };


    // create the vertex buffer
    D3D11_BUFFER_DESC bd;
    ZeroMemory(&bd, sizeof(bd));

    bd.Usage = D3D11_USAGE_DYNAMIC;                // write access access by CPU and GPU
    bd.ByteWidth = sizeof(VERTEX) * 4;             // size is the VERTEX struct * 4
    bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;       // use as a vertex buffer
    bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;    // allow CPU to write in buffer

    dev->CreateBuffer(&bd, NULL, &pVBuffer);       // create the buffer

    // copy the vertices into the buffer
    D3D11_MAPPED_SUBRESOURCE ms;
    devcon->Map(pVBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms);    // map the buffer
    memcpy(ms.pData, rectangle_4_by_3, sizeof(rectangle_4_by_3));                 // copy the data
    devcon->Unmap(pVBuffer, NULL);                                      // unmap the buffer

    HRESULT result;

    //XXX fixed width and height, should adjust properly
    
    //create a GPU resource description of a texture that can be written by the CPU
    D3D11_TEXTURE2D_DESC desc;
    desc.Width = TEXTURE_WIDTH;
    desc.Height = TEXTURE_HEIGHT;
    desc.MipLevels = 1;
    desc.ArraySize = 1;
    desc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
    desc.SampleDesc.Count = 1;
    desc.SampleDesc.Quality = 0;
    desc.Usage = D3D11_USAGE_DYNAMIC;
    desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
    desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    desc.MiscFlags = 0;

	result = dev->CreateTexture2D(&desc, NULL, &pLeftTexture);
	result = dev->CreateTexture2D(&desc, NULL, &pRightTexture);
    if (FAILED(result))
	{
        exit(-1);
	}

    //each texture needs a shader resource view, we use the same BGRA scheme as given by the PS Eye
    D3D11_SHADER_RESOURCE_VIEW_DESC SRVDesc;
    memset(&SRVDesc, 0, sizeof(SRVDesc));
    SRVDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	SRVDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
    SRVDesc.Texture2D.MipLevels = 1;

    result = dev->CreateShaderResourceView(pLeftTexture, &SRVDesc, &pLeftSRV);
    result = dev->CreateShaderResourceView(pRightTexture, &SRVDesc, &pRightSRV);

    if (FAILED(result))
	{
        exit(-2);
	}
}

//this function loads and prepares the shaders
void InitPipeline()
{
    //load and compile the shaders
    ID3D10Blob *VS, *PS, *PS_right;
    D3DX11CompileFromFile(L"shaders.hlsl", 0, 0, "VShader", "vs_5_0", 0, 0, 0, &VS, 0, 0);
    D3DX11CompileFromFile(L"shaders.hlsl", 0, 0, "pixel_shader_left", "ps_5_0", 0, 0, 0, &PS, 0, 0);
    D3DX11CompileFromFile(L"shaders.hlsl", 0, 0, "pixel_shader_right", "ps_5_0", 0, 0, 0, &PS_right, 0, 0);

    //encapsulate both shaders into shader objects
    dev->CreateVertexShader(VS->GetBufferPointer(), VS->GetBufferSize(), NULL, &vertex_shader);
    dev->CreatePixelShader(PS->GetBufferPointer(), PS->GetBufferSize(), NULL, &pixel_shader_left);
    dev->CreatePixelShader(PS_right->GetBufferPointer(), PS_right->GetBufferSize(), NULL, &pixel_shader_right);

    //set the shader objects
    devcon->VSSetShader(vertex_shader, 0, 0);
    devcon->PSSetShader(pixel_shader_left, 0, 0);

    //create the input layout object
    D3D11_INPUT_ELEMENT_DESC ied[] =
    {
        {"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0},
		{"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0}
    };

    dev->CreateInputLayout(ied, 2, VS->GetBufferPointer(), VS->GetBufferSize(), &pLayout);
    devcon->IASetInputLayout(pLayout);

    D3D11_BUFFER_DESC bd;
    ZeroMemory(&bd, sizeof(bd));

    bd.Usage = D3D11_USAGE_DEFAULT;
    bd.ByteWidth = 64;
    bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

    dev->CreateBuffer(&bd, NULL, &pCBuffer);
    devcon->VSSetConstantBuffers(0, 1, &pCBuffer);

    //Initialize the hmd visual parameters
	hmd_params.lens_offset_left = DirectX::XMFLOAT2(DEFAULT_LENS_OFFSET_LEFT_X, DEFAULT_LENS_OFFSET_LEFT_Y);
	hmd_params.lens_offset_right = DirectX::XMFLOAT2(DEFAULT_LENS_OFFSET_RIGHT_X, DEFAULT_LENS_OFFSET_RIGHT_Y);
	hmd_params.texture_scale_left = DirectX::XMFLOAT2(DEFAULT_TEXTURE_SCALE_LEFT_X, DEFAULT_TEXTURE_SCALE_LEFT_Y);
	hmd_params.texture_scale_right = DirectX::XMFLOAT2(DEFAULT_TEXTURE_SCALE_RIGHT_X, DEFAULT_TEXTURE_SCALE_RIGHT_Y);
    hmd_params.k_values_left = DirectX::XMFLOAT4(DEFAULT_K_VALUES_LEFT_W, DEFAULT_K_VALUES_LEFT_Y, 
		                                         DEFAULT_K_VALUES_LEFT_Z, DEFAULT_K_VALUES_LEFT_W);
    hmd_params.k_values_right = DirectX::XMFLOAT4(DEFAULT_K_VALUES_RIGHT_W, DEFAULT_K_VALUES_RIGHT_Y, 
		                                         DEFAULT_K_VALUES_RIGHT_Z, DEFAULT_K_VALUES_RIGHT_W);
    //primarily these are the only parameters adjusted to align the images to create the 3D affect
    hmd_params.texture_offset_left = DirectX::XMFLOAT2(DEFAULT_TEXTURE_OFFSET_LEFT_X, DEFAULT_TEXTURE_OFFSET_LEFT_Y);
    hmd_params.texture_offset_right = DirectX::XMFLOAT2(DEFAULT_TEXTURE_OFFSET_RIGHT_X, DEFAULT_TEXTURE_OFFSET_RIGHT_Y);

    //we create a GPU subresource constant buffer where the CPU can adjust these values that will be used in the shaders
    D3D11_BUFFER_DESC desc;
    ZeroMemory(&desc, sizeof(desc));
    desc.ByteWidth = sizeof(hmd_params_t);
	desc.Usage = D3D11_USAGE_DYNAMIC;
    desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    desc.MiscFlags = 0;
    desc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA init_data;
    init_data.pSysMem = &hmd_params;
    init_data.SysMemPitch = 0;
    init_data.SysMemSlicePitch = 0;

    HRESULT result = dev->CreateBuffer(&desc, &init_data, &hmd_params_buffer);

    if (FAILED(result))
	{
        exit(-2);
	}

    //sets the hmd_warp_params_cbuffer constant buffer in shaders.hlsl
	devcon->PSSetConstantBuffers(1, 1, &hmd_params_buffer);
}

void handle_key_presses()
{
    if (kb->key_pressed(VK_CONTROL))
    {
        allow_adjustments = !allow_adjustments;
    }

    if (!allow_adjustments)
	{
        return;
	}
    //switch adjustment modes
    if (kb->key_pressed(VK_1))
	{
        adjusting = ADJUST_TEXTURE_OFFSET_LEFT;
	}
    else if (kb->key_pressed(VK_2))
	{
        adjusting = ADJUST_TEXTURE_OFFSET_RIGHT;
	}
	else if (kb->key_pressed(VK_3))
	{
        adjusting = ADJUST_TEXTURE_SCALE_LEFT;
	}
	else if (kb->key_pressed(VK_4))
	{
        adjusting = ADJUST_TEXTURE_SCALE_RIGHT;
	}
	else if (kb->key_pressed(VK_5))
	{
        adjusting = ADJUST_K_VALUES_LEFT;
	}
	else if (kb->key_pressed(VK_6))
	{
        adjusting = ADJUST_K_VALUES_RIGHT;
	}
	else if (kb->key_pressed(VK_7))
	{
        adjusting = ADJUST_LENS_OFFSET_LEFT;
	}
	else if (kb->key_pressed(VK_8))
	{
        adjusting = ADJUST_LENS_OFFSET_RIGHT;
	}
    
    if (kb->key_pressed(VK_LEFT))
	{
        switch (adjusting)
		{
            //shift left image to the left
            case ADJUST_TEXTURE_OFFSET_LEFT:
                hmd_params.texture_offset_left.x += TEXTURE_OFFSET_DELTA;
                break;
            //shift right image to the left
            case ADJUST_TEXTURE_OFFSET_RIGHT:
                hmd_params.texture_offset_right.x += TEXTURE_OFFSET_DELTA;
                break;
            //decrease the left image texture scale
            case ADJUST_TEXTURE_SCALE_LEFT:
                hmd_params.texture_scale_left.x -= TEXTURE_SCALE_DELTA;
                break;
            //decrease the right image texture scale
            case ADJUST_TEXTURE_SCALE_RIGHT:
                hmd_params.texture_scale_right.x -= TEXTURE_SCALE_DELTA;
                break;
            //move the left lens warp to the left
            case ADJUST_LENS_OFFSET_LEFT:
                hmd_params.lens_offset_left.x  -= LENS_OFFSET_DELTA;
                break;
            //move the right lens warp to the left
            case ADJUST_LENS_OFFSET_RIGHT:
                hmd_params.lens_offset_right.x -= LENS_OFFSET_DELTA;
                break;
		}
	}

    if (kb->key_pressed(VK_RIGHT))
	{
        switch (adjusting)
		{
            case ADJUST_TEXTURE_OFFSET_LEFT:
                hmd_params.texture_offset_left.x -= TEXTURE_OFFSET_DELTA;
                break;
            case ADJUST_TEXTURE_OFFSET_RIGHT:
                hmd_params.texture_offset_right.x -= TEXTURE_OFFSET_DELTA;
                break;
            case ADJUST_TEXTURE_SCALE_LEFT:
                hmd_params.texture_scale_left.x += TEXTURE_SCALE_DELTA;
                break;
            case ADJUST_TEXTURE_SCALE_RIGHT:
                hmd_params.texture_scale_right.x += TEXTURE_SCALE_DELTA;
                break;
            case ADJUST_LENS_OFFSET_LEFT:
                hmd_params.lens_offset_left.x  += LENS_OFFSET_DELTA;
                break;
            case ADJUST_LENS_OFFSET_RIGHT:
                hmd_params.lens_offset_right.x += LENS_OFFSET_DELTA;
                break;
		}
	}

    if (kb->key_pressed(VK_UP))
	{
        switch (adjusting)
		{
            case ADJUST_TEXTURE_OFFSET_LEFT:
                hmd_params.texture_offset_left.y += TEXTURE_OFFSET_DELTA;
                break;
            case ADJUST_TEXTURE_OFFSET_RIGHT:
                hmd_params.texture_offset_right.y += TEXTURE_OFFSET_DELTA;
                break;
            case ADJUST_TEXTURE_SCALE_LEFT:
                hmd_params.texture_scale_left.y -= TEXTURE_SCALE_DELTA;
                break;
            case ADJUST_TEXTURE_SCALE_RIGHT:
                hmd_params.texture_scale_right.y -= TEXTURE_SCALE_DELTA;
                break;
            //increase the left lens k values
			case ADJUST_K_VALUES_LEFT:
                hmd_params.k_values_left.x += K_VALUES_DELTA;
                hmd_params.k_values_left.y += K_VALUES_DELTA;
                hmd_params.k_values_left.z += K_VALUES_DELTA;
                hmd_params.k_values_left.w += K_VALUES_DELTA;
                break;
            //increase the right lens k values
			case ADJUST_K_VALUES_RIGHT:
                hmd_params.k_values_right.x += K_VALUES_DELTA;
                hmd_params.k_values_right.y += K_VALUES_DELTA;
                hmd_params.k_values_right.z += K_VALUES_DELTA;
                hmd_params.k_values_right.w += K_VALUES_DELTA;
                break;
            case ADJUST_LENS_OFFSET_LEFT:
                hmd_params.lens_offset_left.y  += LENS_OFFSET_DELTA;
                break;
            case ADJUST_LENS_OFFSET_RIGHT:
                hmd_params.lens_offset_right.y += LENS_OFFSET_DELTA;
                break;
		}
	}

    if (kb->key_pressed(VK_DOWN))
	{
        switch (adjusting)
		{
            case ADJUST_TEXTURE_OFFSET_LEFT:
                hmd_params.texture_offset_left.y -= TEXTURE_OFFSET_DELTA;
                break;
            case ADJUST_TEXTURE_OFFSET_RIGHT:
                hmd_params.texture_offset_right.y -= TEXTURE_OFFSET_DELTA;
                break;
            case ADJUST_TEXTURE_SCALE_LEFT:
                hmd_params.texture_scale_left.y += TEXTURE_SCALE_DELTA;
                break;
            case ADJUST_TEXTURE_SCALE_RIGHT:
                hmd_params.texture_scale_right.y += TEXTURE_SCALE_DELTA;
                break;
			case ADJUST_K_VALUES_LEFT:
                hmd_params.k_values_left.x -= K_VALUES_DELTA;
                hmd_params.k_values_left.y -= K_VALUES_DELTA;
                hmd_params.k_values_left.z -= K_VALUES_DELTA;
                hmd_params.k_values_left.w -= K_VALUES_DELTA;
                break;
			case ADJUST_K_VALUES_RIGHT:
                hmd_params.k_values_right.x -= K_VALUES_DELTA;
                hmd_params.k_values_right.y -= K_VALUES_DELTA;
                hmd_params.k_values_right.z -= K_VALUES_DELTA;
                hmd_params.k_values_right.w -= K_VALUES_DELTA;
                break;
            case ADJUST_LENS_OFFSET_LEFT:
                hmd_params.lens_offset_left.y  -= LENS_OFFSET_DELTA;
                break;
            case ADJUST_LENS_OFFSET_RIGHT:
                hmd_params.lens_offset_right.y -= LENS_OFFSET_DELTA;
                break;
		}
	}

    bool adjustment_made = false;
	if (kb->key_pressed(VK_UP) || kb->key_pressed(VK_DOWN) || kb->key_pressed(VK_LEFT) || kb->key_pressed(VK_RIGHT))
	{
		adjustment_made = true;
	}

    if (kb->key_pressed(VK_SPACE))
	{
        mirroring = !mirroring;
	}

    if (mirroring)
    {
        hmd_params.lens_offset_right.y = hmd_params.lens_offset_left.y;
        hmd_params.lens_offset_right.x = -hmd_params.lens_offset_left.x;
        hmd_params.texture_scale_right = hmd_params.texture_scale_left;
        hmd_params.k_values_right = hmd_params.k_values_left;
        hmd_params.texture_offset_right.y = hmd_params.texture_offset_left.y;
        hmd_params.texture_offset_right.x = -hmd_params.texture_offset_left.x;
    }

    if (adjustment_made)
	{
        //allow the CPU to write the hmd paramaters to the GPU
        D3D11_MAPPED_SUBRESOURCE warp_params;
        devcon->Map(hmd_params_buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &warp_params);
        memcpy(warp_params.pData, &hmd_params, sizeof(hmd_params_t));
        devcon->Unmap(hmd_params_buffer, 0);

        printf("-----------------VALUES-----------------\n");
        printf("lens_offset_left (%.05f, %.05f)\n", hmd_params.lens_offset_left.x, hmd_params.lens_offset_left.y);
        printf("lens_offset_right (%.05f, %.05f)\n", hmd_params.lens_offset_right.x, hmd_params.lens_offset_right.y);
        printf("texture_scale_left (%.05f, %.05f)\n", hmd_params.texture_scale_left.x, hmd_params.texture_scale_right.y);
		printf("texture_scale_right (%.05f, %.05f)\n", hmd_params.texture_scale_right.x, hmd_params.texture_offset_right.y);
        printf("k_values_left (%.05f, %.05f, %.05f, %.05f)\n", hmd_params.k_values_left.x, hmd_params.k_values_left.y, hmd_params.k_values_left.z, hmd_params.k_values_left.w);
        printf("k_values_right (%.05f, %.05f, %.05f , %.05f)\n", hmd_params.k_values_right.x, hmd_params.k_values_right.y, hmd_params.k_values_right.z, hmd_params.k_values_right.w);
        printf("texture_offset_left (%.05f, %.05f)\n", hmd_params.texture_offset_left.x, hmd_params.texture_offset_left.y);
        printf("texture_offset_right (%.05f, %.05f)\n", hmd_params.texture_offset_right.x, hmd_params.texture_offset_right.y);
	}
}

// this is the function used to render a single frame
void RenderFrame(image_info_t *image_info_left, image_info_t *image_info_right, unsigned char *left_image, unsigned char *right_image)
{
    D3DXMATRIX matRotate, matView, matProjection, matFinal;

    //static float Time = 0.0f; Time += 0.001f;
    static int last_buffer_index_left = 0;
    static int last_buffer_index_right = 0;

    //update texture whenever we've read off a new image
    if (image_info_left->buffer_index != last_buffer_index_left)
	{
        //allow the CPU to write the image buffer to the texture buffer on the GPU
        D3D11_MAPPED_SUBRESOURCE texture_ms;
        devcon->Map(pLeftTexture, 0, D3D11_MAP_WRITE_DISCARD, 0, &texture_ms);
        //XXX this should somehow take into account image_info's width and height
        memcpy(texture_ms.pData, left_image, TEXTURE_WIDTH * 4 * TEXTURE_HEIGHT);
        devcon->Unmap(pLeftTexture, 0);

        last_buffer_index_left = image_info_left->buffer_index;
	}

    //update texture whenever we've read off a new image
    if (image_info_right->buffer_index != last_buffer_index_right)
	{
        //allow the CPU to write the image buffer to the texture buffer on the GPU
        D3D11_MAPPED_SUBRESOURCE texture_ms;
        devcon->Map(pRightTexture, 0, D3D11_MAP_WRITE_DISCARD, 0, &texture_ms);
        //XXX this should somehow take into account image_info's width and height
        memcpy(texture_ms.pData, right_image, TEXTURE_WIDTH * 4 * TEXTURE_HEIGHT);
        devcon->Unmap(pRightTexture, 0);

        last_buffer_index_right = image_info_right->buffer_index;
	}


    //key presses are used to adjust the hmd warping and texture offsets
    kb->get_keyboard_state();
    handle_key_presses();

    // create a rotation matrix
    D3DXMatrixRotationY(&matRotate, 0);

    D3DXMatrixLookAtLH(&matView,
                       &D3DXVECTOR3(0.0f, 0.0f, 0.0f),    // the camera position
                       &D3DXVECTOR3(0.0f, 0.0f, 1.0f),    // the look-at position
                       &D3DXVECTOR3(0.0f, 1.0f, 0.0f));   // the up direction

    // create a projection matrix
    D3DXMatrixPerspectiveFovLH(&matProjection,
                               (FLOAT)D3DXToRadian(45),                    // field of view
                               (FLOAT)SCREEN_WIDTH / 2.0f / (FLOAT)SCREEN_HEIGHT, // aspect ratio
                               0.01f,                                       // near view-plane
                               100.0f);                                    // far view-plane


    // create the final transform
    matFinal = matRotate * matView * matProjection;

    // set the new values for the constant buffer
    devcon->UpdateSubresource(pCBuffer, 0, 0, &matFinal, 0, 0);

    // clear the back buffer to a deep blue
    devcon->ClearRenderTargetView(backbuffer, D3DXCOLOR(0.0f, 0.2f, 0.4f, 1.0f));

        // select which vertex buffer to display
        UINT stride = sizeof(VERTEX);
        UINT offset = 0;
        devcon->IASetVertexBuffers(0, 1, &pVBuffer, &stride, &offset);

        // select which primtive type we are using
        devcon->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

        //set the texture and viewport and draw the rectangle
        //use different shaders for left and right because they are mirrored params. ie. right = -left;

        //draw the left image
        devcon->PSSetShader(pixel_shader_left, 0, 0);
        devcon->PSSetShaderResources(0, 1, &pLeftSRV);
        devcon->RSSetViewports(1, &viewports[0]);
        devcon->Draw(4, 0);

        //draw the right image
        devcon->PSSetShader(pixel_shader_right, 0, 0);
        devcon->RSSetViewports(1, &viewports[1]);
        devcon->PSSetShaderResources(0, 1, &pRightSRV);
        devcon->Draw(4, 0);

    // switch the back buffer and the front buffer
    swapchain->Present(VSYNC, 0);
}

// this is the function that cleans up Direct3D and COM
void CleanD3D(void)
{
    swapchain->SetFullscreenState(FALSE, NULL);    // switch to windowed mode

    // close and release all existing COM objects
    pLayout->Release();
    vertex_shader->Release();
    pixel_shader_left->Release();
    pixel_shader_right->Release();
    pVBuffer->Release();
    pCBuffer->Release();
    pLeftSRV->Release();
    pRightSRV->Release();
    swapchain->Release();
    backbuffer->Release();
    dev->Release();
    devcon->Release();
}
