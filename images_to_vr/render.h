#ifndef __RENDER_H__
#define __RENDER_H__

#include "image_buffer/image_buffer.h"

#include <Windows.h>

#define SCREEN_WIDTH 1280
#define SCREEN_HEIGHT 800

#define TEXTURE_WIDTH 640
#define TEXTURE_HEIGHT 480

#define WINDOWED TRUE
#define VSYNC 1

//lens offset and k values are described in the Oculus SDK
#define DEFAULT_LENS_OFFSET_LEFT_X 0.1453f
#define DEFAULT_LENS_OFFSET_LEFT_Y 0.0f
#define DEFAULT_LENS_OFFSET_RIGHT_X -0.1453f
#define DEFAULT_LENS_OFFSET_RIGHT_Y 0.0f
#define DEFAULT_TEXTURE_SCALE_LEFT_X 0.05f
#define DEFAULT_TEXTURE_SCALE_LEFT_Y 0.05f
#define DEFAULT_TEXTURE_SCALE_RIGHT_X 0.05f
#define DEFAULT_TEXTURE_SCALE_RIGHT_Y 0.05f
#define DEFAULT_K_VALUES_LEFT_X 1.0f
#define DEFAULT_K_VALUES_LEFT_Y 0.18f
#define DEFAULT_K_VALUES_LEFT_Z 0.115f
#define DEFAULT_K_VALUES_LEFT_W 0.0f
#define DEFAULT_K_VALUES_RIGHT_X 1.0f
#define DEFAULT_K_VALUES_RIGHT_Y 0.18f
#define DEFAULT_K_VALUES_RIGHT_Z 0.115f
#define DEFAULT_K_VALUES_RIGHT_W 0.0f
#define DEFAULT_TEXTURE_OFFSET_LEFT_X 0.0f
#define DEFAULT_TEXTURE_OFFSET_LEFT_Y 0.0f
#define DEFAULT_TEXTURE_OFFSET_RIGHT_X 0.0f
#define DEFAULT_TEXTURE_OFFSET_RIGHT_Y 0.0f

#define LENS_OFFSET_DELTA 0.01f
#define TEXTURE_SCALE_DELTA 0.01f;
#define K_VALUES_DELTA 0.03f;
#define TEXTURE_OFFSET_DELTA 0.001f;

#define ADJUST_TEXTURE_OFFSET_LEFT  0
#define ADJUST_TEXTURE_OFFSET_RIGHT 1
#define ADJUST_TEXTURE_SCALE_LEFT   2
#define ADJUST_TEXTURE_SCALE_RIGHT  3
#define ADJUST_K_VALUES_LEFT        4
#define ADJUST_K_VALUES_RIGHT       5
#define ADJUST_LENS_OFFSET_LEFT     6
#define ADJUST_LENS_OFFSET_RIGHT    7

// function prototypes
void InitD3D(HWND hWnd);                       //sets up and initializes Direct3D
void RenderFrame(image_info_t *image_info_left,     //renders the current images
                 image_info_t *image_info_right,
				 unsigned char *left_image, 
				 unsigned char *right_image);     
void CleanD3D(void);                           //closes Direct3D and releases memory



#endif
